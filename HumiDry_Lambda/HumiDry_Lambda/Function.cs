using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.Serialization;

using Alexa.NET;
using Alexa.NET.Response;
using Alexa.NET.Request;
using Alexa.NET.Request.Type;

using Newtonsoft.Json;



// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace HumiDry_Lambda
{
    public class Function
    {
        public SkillResponse FunctionHandler(SkillRequest input, ILambdaContext context)
        {
            SkillResponse response = new SkillResponse();
            response.Response = new ResponseBody();

            response.Response.ShouldEndSession = false;

            IOutputSpeech innerResponse = null;
            var log = context.Logger;

            if (input.GetRequestType() == typeof(LaunchRequest))
            {
                innerResponse = new PlainTextOutputSpeech();
                (innerResponse as PlainTextOutputSpeech).Text = "Hello, HumiDry Voice Control Just Launched!";
            }
            else if (input.GetRequestType() == typeof(IntentRequest))
            {
                var intentRequest = (IntentRequest)input.Request;

                string slotID = intentRequest.Intent.Slots["HumiDry_Modes"].Resolution.Authorities[0].Values[0].Value.Id;
               // log.LogLine(slotID);
//                int SlotID = Int32.Parse(slotID);
               //SlotID.ToString();

                switch (intentRequest.Intent.Name)
                {
                    case "Amazon.CancelIntent":
                        innerResponse = new PlainTextOutputSpeech();
                        (innerResponse as PlainTextOutputSpeech).Text = "Goodbye!";
                        response.Response.ShouldEndSession = true;
                        break;
                    case "Amazon.StopIntent":
                        innerResponse = new PlainTextOutputSpeech();
                        (innerResponse as PlainTextOutputSpeech).Text = "Goodbye!";
                        response.Response.ShouldEndSession = true;
                        break;
                    case "AMAZON.HelpIntent":
                        innerResponse = new PlainTextOutputSpeech();
                        (innerResponse as PlainTextOutputSpeech).Text = "You can ask HumiDry to activate desired operation.";
                        break;
                    case "SetMode":
                         switch (slotID)
                         {
                             case "0":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "HumiDry has been turned OFF";
                                 break;
                             case "1":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "Smart Room Heating Activated";
                                 break;
                             case "2":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "Cool Air Dehumidifying Activated";
                                 break;
                             case "3":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "Enhanced Dehumidifying Activated";
                                 break;
                             case "4":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "Smart Clothes Drying Activated";
                                 break;
                             case "5":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "Air Venting Activated";
                                 break;
                             case "6":
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "Air Recirculation Activated";
                                 break;
                             default:
                                innerResponse = new PlainTextOutputSpeech();
                                (innerResponse as PlainTextOutputSpeech).Text = "exception occured";
                                 break;

                        }
                        //response.Response.ShouldEndSession = true;
                        break;
                    default:
                        innerResponse = new PlainTextOutputSpeech();
                        (innerResponse as PlainTextOutputSpeech).Text = "What can I assist you with?";
                        break;
                }
            }

            response.Response.OutputSpeech = innerResponse;
            response.Version = "1.0";

            return response;














        }




        
    }
}
